import axios from "axios/index";
import CookieOperations from "./CookieOperationsCollector";

/**
* @usage: retrieving and setting language packs
*
* > getLangPack
* @work: tries to make an ajax request to the route /langPack and gives response to full object
* @success: language pack object {lang, langPackFields} or {lang, error(E1000-1)}
* @fail: set error code E1000
* > setLang
* @work: sets cookies with information about preferred language pack then reloads the page desiring node  to proceed with the cookie
 */

export default class LangCollector {
    static async getLangPack(lang){
        var full = {}
        await axios({
            method: 'post',
            url: process.env.npm_package_proxy + '/langPack',
            data: {
                lang: lang
            }
        }).then(res => res.data).then(
            res => full = res
        ).catch(() => full = {error: "E1000"})

        return full
    }
    static setLang(lang, duration){
        if(CookieOperations.get().lang != lang) {
            CookieOperations.set("lang", lang, duration)
            location.reload()
        }
    }
}