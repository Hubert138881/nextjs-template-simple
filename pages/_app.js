import App from 'next/app'
import Head from 'next/head'
import Router from 'next/router'

import React from 'react'
import "../styles/styleReset.css"
import Lang from "../components/collectors/LangCollector"

const defaultLanguage = "en"

/*
* W tym pliku można wykonywać operacje na danych ktore otrzymalismy z getInitialProps elementow
* Także ustala sie zawartosc tagu <head>
*/

export default class MyApp extends App {
    static async getInitialProps({ Component, ctx }) {
        let pageProps = {}
        if (Component.getInitialProps) pageProps = await Component.getInitialProps(ctx)

        let dataPack = await Lang.getLangPack(pageProps.lang);
        if(dataPack.error) return dataPack

        pageProps.currPage = ctx.asPath
        return {pageProps, serverData: dataPack.serverData, langPack: dataPack.langPack}
    }

    render() {
        const {Component, pageProps, serverData, langPack} = this.props;

        switch(this.props.error) {
            case "E1000":
                return (<>
                    <Head> <title>Error E1000</title> </Head>
                    <body>(E1000) We were not able to render this page, please try once again and refresh it</body>
                </>)
                break;
            case "E1000-1":
                return (<>
                    <Head> <title>Error E1000-1</title> </Head>
                    <body>(E1000-1) We were not able to render this page, please try once again and refresh it</body>
                </>)
                break;
            default:
                return (<>
                    <Head> <title>Sample title</title> </Head>
                    <Component {...pageProps} serverData={serverData} langPack={langPack}/>
                </>)
        }
    }
}
