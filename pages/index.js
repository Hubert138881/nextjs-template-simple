import React from 'react'
import {appLog, errorLog} from "../components/utilities/logs"

const Home = (props) => {
    return <p>Index: {props.langPack.welcome}</p>
}
Home.getInitialProps = async (props) => {
    return {lang: props.req.params.lang}
}
export default Home