import React from 'react'

const E404 = (props) => {
    return(
        <main>{props.E404}</main>
    )
}

E404.getInitialProps = async (props) => {
    return {lang: props.req.params.lang}
}

export default E404