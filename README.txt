Hello there!

This package contains a basic configuration and some useful tools for next.js, such as:
- multi language support (using cookies and routing)
- logging tools
- easy, multi language error handling
WITHOUT database connection handling (This feature is available in my other package)

To use the multi-language functionality you need to set up some things first:
1. Make sure if the proxy provided in package.json is correct
2. In server/langPack check and create json packages with your translations (only one-leveled jsons will proceed)
3. In server/controllers/LangController.js make sure about the content of defaultLanguage and checkBrowserLanguage variables
4. Check out example of usage in the /pages folder (create all of the pages in the /pages/[lang])
5. In /pages/_app.js define your E1000 and E1000-1 errors handling (two first returns)

To use logs:
a) server-side - use app.appLog.type("message") for standard Logs or app.errorLog.type("message") for errors
b) client-side - import components/utilities/logs.js and use as a function: log("appLog/errorLog", "type", "message")

there were some 'fs' errors so I decided to use another method to log things on client

where "type" is one of those: trace < debug < info < warn < error < fatal

Enjoy :)
Created by Hubert13888 (PL)