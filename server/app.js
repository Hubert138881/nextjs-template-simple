const express = require("express")
const next = require('next')
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")

const port = process.env.PORT || 3000,
      dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

//LOG System
const log4js = require('log4js');
log4js.configure({
    appenders: {
        errors: {type: 'file', filename: 'logs/errorLog.log'},
        app: {type: 'file', filename: 'logs/appLog.log'}
    },
    categories: {
        errors: {appenders: ['errors'], level: ['error']},
        default: {appenders: ['app'], level: ['trace']}
    }
})

app.fs = require("fs")
app.errorLog = log4js.getLogger('errors')
app.appLog = log4js.getLogger('app')
app.serverData = require("./serverData.js")

app
    .prepare()
    .then(() => {
        const server = express()

        server.use(bodyParser.json())
        server.use(bodyParser.urlencoded({ extended: true }))
        server.use(cookieParser())

        var controllerList = ["Log", "Lang"]; //Tu nalezy dopisac kolejne kontrolery
        controllerList.map(name => require(`./controllers/${name}Controller`)(server, app))

        server.get("*", (req, res) => {
            return handle(req, res)
        })

        server.listen(port, err => {
            if(err) throw err
            console.log(`Server's ready on ${port}`)
        })

    })
    .catch(ext => {
        console.error(ext.stack)
        process.exit(1)
    })