module.exports = {
  "welcome": "Welcome",
  "apple": "Apple",
  "peanut": "peanut",

  "E404": "(404) You've got to the road not taken...",
  "E1000": "Language pack Ajax error (Not shown from this file)",
  "E1000-1": "Language pack error connected with node file system (Not shown from this file)",
  "E1001": "Logs error. Something does not work properly"
}